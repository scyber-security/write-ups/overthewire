# [Bandit Level 14 → 15](https://overthewire.org/wargames/bandit/bandit15.html)


## The Goal
The password for the next level can be retrieved by submitting the password of the current level to **port 30000 on localhost**.


## Finding the Password
##### SSH Command
```sh
sshpass -p 4wcYUJFw0k0XLShlDzztnTBHiqxU3b3e ssh bandit.labs.overthewire.org -p 2220 -l bandit14
```

---
Simple case of piping the the password for the *bandit14* user to a service listening on port 30000 on the same host.

```sh
echo 4wcYUJFw0k0XLShlDzztnTBHiqxU3b3e | nc localhost 30000
```

> `BfMYroe26WYalil77FoDi9qh59eK5xNr`
