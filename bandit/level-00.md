# [Bandit Level 0 → 1](https://overthewire.org/wargames/bandit/bandit1.html)


## The Goal
The password for the next level is stored in a file called readme located in the home directory. Use this password to log into bandit1 using SSH. Whenever you find a password for a level, use SSH (on port 2220) to log into that level and continue the game.


## Finding the Password
##### SSH Command
```sh
sshpass -p bandit0 ssh bandit.labs.overthewire.org -p 2220 -l bandit0
```

Simply run `ls` to see the only file in the directory is **readme**. `cat` the file, and there's the password:

> `boJ9jbbUNNfktd78OOpsqOltutMc3MY1`
