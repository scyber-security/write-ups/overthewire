# [Bandit Level 6 → 7](https://overthewire.org/wargames/bandit/bandit7.html)


## The Goal
The password for the next level is stored somewhere on the server and has all of the following properties:
* owned by user bandit7
* owned by group bandit6
* 33 bytes in size


## Finding the Password
##### SSH Command
```sh
sshpass -p DXjZPULLxYr17uwoI01bNLQbtFemEgo7 ssh bandit.labs.overthewire.org -p 2220 -l bandit6
```

---
Using find is the key here: `find / -type f -user bandit7 -group bandit6 -size 33c 2>/dev/null` which gave the file */var/lib/dpkg/info/bandit7.password*. We can add the `-exec` flag at the end to print the contents straight out to the terminal.

> `HKBPTKQnIay4Fw76bEy8PVxKEDQRKTzs`
