# [Bandit Level 4 → 5](https://overthewire.org/wargames/bandit/bandit5.html)


## The Goal
The password for the next level is stored in the only human-readable file in the inhere directory. Tip: if your terminal is messed up, try the “reset” command.


## Finding the Password
##### SSH Command
```sh
sshpass -p pIwrPrtPN36QITSp3EQaw936yaFoFgAB ssh bandit.labs.overthewire.org -p 2220 -l bandit4
```

---

There's a directory called **inhere** which contains files, all with a **-** in front of them. The hint is that only one contains something which is human readable. For this level, the `file` command will be very useful as it will tell you the type of file that you're dealing with.

My first attempt was to run `file *` in the **inhere** directory, but that was a no go; I had forgotten about the **-** problem so soon after completing the previous level. Back up one directory and ran `file inhere/*` to see that the only file which wasn't *data*, was **-file07**.

![The Files!](./images/level-04-file.png)

Running the command `cat inhere/-file07` gives the password of:

> `koReBOKuIDDepwhWk7jZC0RTdopnAYKh`
