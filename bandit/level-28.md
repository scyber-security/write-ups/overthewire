# [Bandit Level 28 → 29](https://overthewire.org/wargames/bandit/bandit29.html)


## The Goal
There is a git repository at **ssh://bandit28-git@localhost/home/bandit28-git/repo**. The password for the user **bandit28-git** is the same as for the user **bandit28**.

Clone the repository and find the password for the next level.


## Finding the Password
##### SSH Command
```sh
sshpass -p 0ef186ac70e04ea33b4c1853d2526fa2 ssh bandit.labs.overthewire.org -p 2220 -l bandit28
```

---
```bash
git clone ssh://bandit28-git@localhost/home/bandit28-git/repo
cat repo/

git log
git checkout c086d11a00c0648d095d04c089786efef5e01264
```


> `bbc96594b4e001778eee9975372716b2`
