# [Bandit Level 3 → 4](https://overthewire.org/wargames/bandit/bandit4.html)


## The Goal
The password for the next level is stored in a hidden file in the inhere directory.


## Finding the Password
##### SSH Command
```sh
sshpass -p UmHadQclWmgdLOKQ3YNgjWxGoRMb5luK ssh bandit.labs.overthewire.org -p 2220 -l bandit3
```

---

Looking at the contents with just `ls inhere` provided no results, however using the *-a* flag to get `ls -a inhere`, the contents of the directory can be viewed. Within this directory is a hidden file called **.hidden**.

Running the command `cat inhere/.hidden` gives the password of:

> `pIwrPrtPN36QITSp3EQaw936yaFoFgAB`
