# [Bandit Level 17 → 18](https://overthewire.org/wargames/bandit/bandit18.html)


## The Goal
There are 2 files in the homedirectory: **passwords.old** and **passwords.new**. The password for the next level is in **passwords.new** and is the only line that has been changed between **passwords.old** and **passwords.new**

NOTE: if you have solved this level and see ‘Byebye!’ when trying to log into bandit18, this is related to the next level, bandit19


## Finding the Password
##### SSH Command
```sh
ssh bandit.labs.overthewire.org -p 2220 -l bandit17 -i level-17.pkey
```

---
Easy one using the `diff` command.

```sh
diff passwords.old passwords.new
```

> `kfBf3eYk5BPBRzwjqutbbfE887SVc5Yd`
