# [Bandit Level 27 → 28](https://overthewire.org/wargames/bandit/bandit28.html)


## The Goal
There is a git repository at **ssh://bandit27-git@localhost/home/bandit27-git/repo**. The password for the user **bandit27-git** is the same as for the user **bandit27**.

Clone the repository and find the password for the next level.


## Finding the Password
##### SSH Command
```sh
sshpass -p 3ba3118a22e93127a4ed485be72ef5ea ssh bandit.labs.overthewire.org -p 2220 -l bandit27
```

---
```bash
git clone ssh://bandit27-git@localhost/home/bandit27-git/repo
cat repo/README
```


> `0ef186ac70e04ea33b4c1853d2526fa2`
