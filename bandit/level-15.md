# [Bandit Level 15 → 16](https://overthewire.org/wargames/bandit/bandit16.html)


## The Goal
The password for the next level can be retrieved by submitting the password of the current level to **port 30001 on localhost** using SSL encryption.

Helpful note: Getting “HEARTBEATING” and “Read R BLOCK”? Use -ign_eof and read the “CONNECTED COMMANDS” section in the manpage. Next to ‘R’ and ‘Q’, the ‘B’ command also works in this version of that command…


## Finding the Password
##### SSH Command
```sh
sshpass -p BfMYroe26WYalil77FoDi9qh59eK5xNr ssh bandit.labs.overthewire.org -p 2220 -l bandit15
```

---
Just like the previous question, but this time using *openssl* to connect to the service and sending the password.

```sh
openssl s_client -connect localhost:30001
```

> `cluFn7wTiGryunymYOu4RcffSxQluehd`
