# [Bandit Level 20 → 21](https://overthewire.org/wargames/bandit/bandit21.html)


## The Goal
There is a setuid binary in the homedirectory that does the following: it makes a connection to localhost on the port you specify as a commandline argument. It then reads a line of text from the connection and compares it to the password in the previous level (bandit20). If the password is correct, it will transmit the password for the next level (bandit21).

NOTE: Try connecting to your own network daemon to see if it works as you think


## Finding the Password
##### SSH Command
```sh
sshpass -p GbKksEFF4yrVs6il55v6gwY5aVje5f0j ssh bandit.labs.overthewire.org -p 2220 -l bandit20
```

---


```sh
# start a netcat listener in one terminal
nc -tlnp 9999

# connect to the listener...
./suconnect 9999

```

> `gE269g2h3mw3pwgrj0Ha9Uoqen1c9DGr`
