# [Bandit Level 21 → 22](https://overthewire.org/wargames/bandit/bandit22.html)


## The Goal
A program is running automatically at regular intervals from **cron**, the time-based job scheduler. Look in **/etc/cron.d/** for the configuration and see what command is being executed.


## Finding the Password
##### SSH Command
```sh
sshpass -p gE269g2h3mw3pwgrj0Ha9Uoqen1c9DGr ssh bandit.labs.overthewire.org -p 2220 -l bandit21
```

---
The hint for this is that a job runs at regular intervals which is achieved using *cron*. 

```sh
ls /etc/cron.d
```

Within this directory are a number of files which are run periodically. The one of interest is `/usr/bin/cronjob_bandit22.sh`. Looking at the contents of this file, we can see that it runs the script `/usr/bin/cronjob_bandit22.sh` which uses `cat` to read the password and redirect it to a file in **/tmp**.


> `Yk7owGAcWjwMVRwrTesJEwB7WVOiILLI`
