# [Bandit Level 25 → 26](https://overthewire.org/wargames/bandit/bandit26.html)


## The Goal
A daemon is listening on port 30002 and will give you the password for bandit25 if given the Logging in to bandit26 from bandit25 should be fairly easy… The shell for user bandit26 is not **/bin/bash**, but something else. Find out what it is, how it works and how to break out of it.


## Finding the Password
##### SSH Command
```sh
sshpass -p uNG9O58gUE7snukf3bvZ0rxhtnjzSGzG ssh bandit.labs.overthewire.org -p 2220 -l bandit25
```

---
```bash
grep bandit26 /etc/passwd
```
> `bandit26:x:11026:11026:bandit level 26:/home/bandit26:/usr/bin/showtext`

```bash
cat /usr/bin/showtext
```
> #!/bin/sh
>
> export TERM=linux
> more ~/text.txt
> exit 0

```bash
ssh -o StrictHostKeyChecking=no -i ~/bandit26.sshkey bandit26@localhost
```

> `5czgV9L3Xx8JPOyRbXh6lQbmIOWvPT6Z`
