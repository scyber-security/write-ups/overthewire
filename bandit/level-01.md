# [Bandit Level 1 → 2](https://overthewire.org/wargames/bandit/bandit2.html)


## The Goal
The password for the next level is stored in a file called - located in the home directory.


## Finding the Password
##### SSH Command
```sh
sshpass -p boJ9jbbUNNfktd78OOpsqOltutMc3MY1 ssh bandit.labs.overthewire.org -p 2220 -l bandit1
```

---

Running `ls`, I can see there's a file by the name of **-**. On it's own, this is a special character in bash. To read this file, you would need to run the command `cat ./-` to get the password of:


> `CV1DtqXWVFXTvM2F0k09SHz0YwRINYA9`
