# [Bandit Level 22 → 23](https://overthewire.org/wargames/bandit/bandit23.html)


## The Goal
A program is running automatically at regular intervals from cron, the time-based job scheduler. Look in **/etc/cron.d/** for the configuration and see what command is being executed.

> **NOTE:** Looking at shell scripts written by other people is a very useful skill. The script for this level is intentionally made easy to read. If you are having problems understanding what it does, try executing it to see the debug information it prints.


## Finding the Password
##### SSH Command
```sh
sshpass -p Yk7owGAcWjwMVRwrTesJEwB7WVOiILLI ssh bandit.labs.overthewire.org -p 2220 -l bandit22
```

---
Taking a look at the `cronjob_bandit23` file in the `/etc/cron.d` directory. Just like the previous question, it references a script in the `/usr/bin` directory; `cronjob_bandit23.sh`

In this script, the password is written to a file name based on the MD5 sum of a string containing the user for the next level.


> `jc1udXuA1tiHqjIsL8yaapX5XIAI6i0n`
