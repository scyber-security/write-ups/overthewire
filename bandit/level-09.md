# [Bandit Level 9 → 10](https://overthewire.org/wargames/bandit/bandit10.html)


## The Goal
The password for the next level is stored in the file **data.txt** in one of the few human-readable strings, preceded by several ‘=’ characters.


## Finding the Password
##### SSH Command
```sh
sshpass -p UsvVyFSfZZWbi6wgC7dAFyFuR6jQQUhR ssh bandit.labs.overthewire.org -p 2220 -l bandit9
```

---
Taking a look at the `data.txt` there is a lot of data in there which isn't human-readable. Luckily, the `strings` command will filter out of all the data that we can't read as humans.

`strings data.txt | grep -E "==="`

> `truKLdjsbJ5g7yyJ2X2R0o3a5HQJFuLk`
