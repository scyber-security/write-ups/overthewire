# [Bandit Level 13 → 14](https://overthewire.org/wargames/bandit/bandit14.html)


## The Goal
The password for the next level is stored in **/etc/bandit_pass/bandit14 and can only be read by user bandit14**. For this level, you don’t get the next password, but you get a private SSH key that can be used to log into the next level. **Note: localhost** is a hostname that refers to the machine you are working on


## Finding the Password
##### SSH Command
```sh
sshpass -p 8ZjyCRiBWFYkneahHwxCv3wb2a1ORpYL ssh bandit.labs.overthewire.org -p 2220 -l bandit13
```

---
In the home directory is a private key. We can use this to ssh to the same machine but as the *bandit14* user.

```sh
ssh bandit14@localhost -i ./sshkey.private

cat /etc/bandit_pass/bandit14
```

> `4wcYUJFw0k0XLShlDzztnTBHiqxU3b3e`
