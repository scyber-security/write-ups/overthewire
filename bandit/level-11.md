# [Bandit Level 11 → 12](https://overthewire.org/wargames/bandit/bandit12.html)


## The Goal
The password for the next level is stored in the file **data.txt**, where all lowercase (a-z) and uppercase (A-Z) letters have been rotated by 13 positions


## Finding the Password
##### SSH Command
```sh
sshpass -p IFukwKGsFW8MOq3IRFqrxE1hxTNEbUPR ssh bandit.labs.overthewire.org -p 2220 -l bandit11
```

---
The data within the text file has been enocded using the ROT13 cipher. Looking up a way to decode this on the command line mentioned the `tr` command to translate text based on *SETS*.

`cat data.txt | tr '[A-Za-z]' '[N-ZA-Mn-za-m]'`

> `5Te8Y4drgCRfCx8ugdwuEX8KFC6k2EUu`
