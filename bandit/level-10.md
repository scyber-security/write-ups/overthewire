# [Bandit Level 10 → 11](https://overthewire.org/wargames/bandit/bandit11.html)


## The Goal
The password for the next level is stored in the file **data.txt**, which contains base64 encoded data.


## Finding the Password
##### SSH Command
```sh
sshpass -p truKLdjsbJ5g7yyJ2X2R0o3a5HQJFuLk ssh bandit.labs.overthewire.org -p 2220 -l bandit10
```

---
Simple case of using the `base64` command to decode the text within the file.

`cat data.txt | base64 -d`

> `IFukwKGsFW8MOq3IRFqrxE1hxTNEbUPR`
