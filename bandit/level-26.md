# [Bandit Level 26 → 27](https://overthewire.org/wargames/bandit/bandit27.html)


## The Goal
Good job getting a shell! Now hurry and grab the password for bandit27!


## Finding the Password
##### SSH Command
```sh
sshpass -p 5czgV9L3Xx8JPOyRbXh6lQbmIOWvPT6Z ssh bandit.labs.overthewire.org -p 2220 -l bandit26
```

---
```vim
:set shell=/bin/bash
:! ./bandit27-do cat /etc/bandit_pass/bandit27
```


> `3ba3118a22e93127a4ed485be72ef5ea`
