# [Bandit Level 7 → 8](https://overthewire.org/wargames/bandit/bandit8.html)


## The Goal
The password for the next level is stored in the file **data.txt** next to the word millionth


## Finding the Password
##### SSH Command
```sh
sshpass -p HKBPTKQnIay4Fw76bEy8PVxKEDQRKTzs ssh bandit.labs.overthewire.org -p 2220 -l bandit7
```

---
This is a simple example of using grep to find the password. The command `grep millionth data.txt` gave the password.

> `cvX2JJa4CFALtqS87jk27qwqGhBM9plV`
