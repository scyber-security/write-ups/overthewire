# [Bandit Level 8 → 9](https://overthewire.org/wargames/bandit/bandit9.html)


## The Goal
The password for the next level is stored in the file **data.txt** and is the only line of text that occurs only once


## Finding the Password
##### SSH Command
```sh
sshpass -p cvX2JJa4CFALtqS87jk27qwqGhBM9plV ssh bandit.labs.overthewire.org -p 2220 -l bandit8
```

---
To get to this password we need to find the unique entry in the file. But before doing this, that data would need to be sorted. `sort` does have the ability to display only the unique values, but doesn't have an option to display the count like the `uniq` command.

`sort data.txt | uniq -c`

> `UsvVyFSfZZWbi6wgC7dAFyFuR6jQQUhR`
