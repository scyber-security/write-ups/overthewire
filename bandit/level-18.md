# [Bandit Level 18 → 19](https://overthewire.org/wargames/bandit/bandit19.html)


## The Goal
The password for the next level is stored in a file readme in the homedirectory. Unfortunately, someone has modified .bashrc to log you out when you log in with SSH.


## Finding the Password
##### SSH Command
```sh
sshpass -p kfBf3eYk5BPBRzwjqutbbfE887SVc5Yd ssh bandit.labs.overthewire.org -p 2220 -l bandit18
```

---
As soon as you try to login to this user, you're immediately signed out with message of `Byebye !`. One of the things with *SSH* is you can send commands which will be executed before the `.bashrc` file is read.


```sh
sshpass -p kfBf3eYk5BPBRzwjqutbbfE887SVc5Yd ssh bandit.labs.overthewire.org -p 2220 -l bandit18 -C ls

sshpass -p kfBf3eYk5BPBRzwjqutbbfE887SVc5Yd ssh bandit.labs.overthewire.org -p 2220 -l bandit18 -C "cat readme"
```

> `IueksS7Ubh8G3DCwVzrTd8rAVOwq3M5x`
