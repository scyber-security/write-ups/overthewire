# [Bandit Level 23 → 24](https://overthewire.org/wargames/bandit/bandit24.html)


## The Goal
A program is running automatically at regular intervals from cron, the time-based job scheduler. Look in **/etc/cron.d/** for the configuration and see what command is being executed.

> NOTE: This level requires you to create your own first shell-script. This is a very big step and you should be proud of yourself when you beat this level!

> NOTE 2: Keep in mind that your shell script is removed once executed, so you may want to keep a copy around…


## Finding the Password
##### SSH Command
```sh
sshpass -p jc1udXuA1tiHqjIsL8yaapX5XIAI6i0n ssh bandit.labs.overthewire.org -p 2220 -l bandit23
```

---



```sh
#!/bin/bash
mkdir -p /tmp/chaos
cat /etc/bandit_pass/bandit24 > /tmp/chaos/pass
```


> `UoMYTrfrBFHyQXmg6gzctqAwOmw1IohZ`
