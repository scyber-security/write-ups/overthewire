# [Bandit Level 19 → 20](https://overthewire.org/wargames/bandit/bandit20.html)


## The Goal
To gain access to the next level, you should use the setuid binary in the homedirectory. Execute it without arguments to find out how to use it. The password for this level can be found in the usual place (/etc/bandit_pass), after you have used the setuid binary.


## Finding the Password
##### SSH Command
```sh
sshpass -p IueksS7Ubh8G3DCwVzrTd8rAVOwq3M5x ssh bandit.labs.overthewire.org -p 2220 -l bandit19
```

---
Within the home directory is an executable with the `setuid` bit set. When this executes, it will execute as if it was the user *bandit20* executing.


```sh
./bandit20-do cat /etc/bandit_pass/bandit20
```

> `GbKksEFF4yrVs6il55v6gwY5aVje5f0j`
