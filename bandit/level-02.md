# [Bandit Level 2 → 3](https://overthewire.org/wargames/bandit/bandit3.html)


## The Goal
The password for the next level is stored in a file called spaces in this filename located in the home directory.


## Finding the Password
##### SSH Command
```sh
sshpass -p CV1DtqXWVFXTvM2F0k09SHz0YwRINYA9 ssh bandit.labs.overthewire.org -p 2220 -l bandit2
```

---

Running `ls`, we are in fact dealing with a file spaces in it. If I were to pass the name of this file into `cat`, it would treat each word as an argument, and look for those files in the current directory. This can be easily solved using one of two methods:

1. `cat "spaces in this filename"`
2. `cat spaces\ in\ this\ filename`

Either way, the password is:

> `UmHadQclWmgdLOKQ3YNgjWxGoRMb5luK`
