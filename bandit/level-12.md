# [Bandit Level 12 → 13](https://overthewire.org/wargames/bandit/bandit13.html)


## The Goal
The password for the next level is stored in the file **data.txt**, which is a hexdump of a file that has been repeatedly compressed. For this level it may be useful to create a directory under /tmp in which you can work using mkdir. For example: mkdir /tmp/myname123. Then copy the datafile using cp, and rename it using mv (read the manpages!)


## Finding the Password
##### SSH Command
```sh
sshpass -p 5Te8Y4drgCRfCx8ugdwuEX8KFC6k2EUu ssh bandit.labs.overthewire.org -p 2220 -l bandit12
```

---
This is quite the tricky one as it's multiple levels of compression, but the commands are as follows:

```sh
xxd -r data.txt out
file out

# gzip compressed data, was "data2.bin"
cp out data2.bin.gz
gunzip data2.gz

file data2.bin
# displays bzip2 compressed
mv data2.bin data2.bzip
bunzip2 data2.bzip

file data2.bzip.out
# gzip compressed data, was "data4.bin"

mv data2.bzip.out data4.bin.gz
file data4.bin
# POSIX tar archive (GNU)

tar xf data4.bin
file data5.bin
# POSIX tar archive (GNU)

tar xf data5.bin
file data6.bin
# bzip2 compressed data

tar xf data6.bin
file data8.bin
# gzip compressed data, was "data9.bin"

mv data8.bin data8.bin.gz
gunzip data8.bin.gz
file data8.bin
# ASCII text

cat data8.bin
```

> `8ZjyCRiBWFYkneahHwxCv3wb2a1ORpYL`
