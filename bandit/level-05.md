# [Bandit Level 5 → 6](https://overthewire.org/wargames/bandit/bandit6.html)


## The Goal
The password for the next level is stored in a file somewhere under the inhere directory and has all of the following properties:
* human-readable
* 1033 bytes in size
* not executable


## Finding the Password
##### SSH Command
```sh
sshpass -p koReBOKuIDDepwhWk7jZC0RTdopnAYKh ssh bandit.labs.overthewire.org -p 2220 -l bandit5
```

---
Running `ls inhere`, I could see that there are 20 directories within. When I ran `ls inhere/*`, I could see quite a few files under each of these sub-directories.

To find the file which matched the above criteria, the following command would be needed: `find inhere/ -type f -size 1033c ! -executable -exec cat {} \;`. The flag `! -executable` was not needed, as the file type and size were more than enough to find one file which matched that criteria: **inhere/maybehere07/.file2**.

The output of the find command gave the following password:

> `DXjZPULLxYr17uwoI01bNLQbtFemEgo7`
